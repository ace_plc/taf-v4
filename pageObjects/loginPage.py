

from testData.config import elements
from testFramework.webapp import webapp



class LoginPage():
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = LoginPage()
        return cls.instance

    def __init__(self):
        self.driver = webapp.get_driver()

    def verify_status(self, row):
        # Ex:
        # status = self.driver.find_element_by_id('dashboard-status-component').text
        # assert row in status, "{} not present in status component".format(row)
        print('Verifying dashboard status..')


    def type_username_password(self, username, password):
        login_username_name = str(elements["login_username_name"])
        login_password_name = str(elements["login_password_name"])
        username_text_field = self.driver.find_element_by_name(name=login_username_name)
        username_text_field.send_keys(username)
        password_text_field = self.driver.find_element_by_name(name=login_password_name)
        password_text_field.send_keys(password)
        print('waiting for 10 seconds')

    def login_to_dashboard(self):
        # Ex:
        login_button_xpath = str(elements['login_button_xpath'])
        login_button = self.driver.find_element_by_xpath(xpath=login_button_xpath)
        login_button.click()
        # assert row in status, "{} not present in status component".format(row)
        print('login button clicked')


    def logout_from_site(self):
        # Ex:
        general_pages_button_xpath = str(elements['general_pages_button_xpath'])
        logout_button_xpath = str(elements['logout_button_xpath'])
        general_pages_button_xpath = self.driver.find_element_by_xpath(xpath=general_pages_button_xpath)
        logout_button = self.driver.find_element_by_xpath(xpath=logout_button_xpath)
        general_pages_button_xpath.click()
        logout_button.click()
        # assert row in status, "{} not present in status component".format(row)
        print('login button clicked')



loginPage = LoginPage.get_instance()