from behave import then, when, given
from testFramework.webapp import webapp
from testData.config import settings



@given(u'I load the website')
def step_impl(context):
    webapp.load_website()


@when(u'I go to "Dashboard" page')
def step_impl(context):
    webapp.goto_page("dashboard")


@then(u'I see this component "Status"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then I see this component "Status"')


@then(u'I see this component "Detector Settings"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then I see this component "Detector Settings"')


@then(u'I see this component "Battery"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then I see this component "Battery"')


@then(u'I see this component "GPS"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then I see this component "GPS"')


@then(u'Dashboard Status shows correct values for row "Status"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Status"')


@then(u'Dashboard Status shows correct values for row "White Reference Count"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "White Reference Count"')


@then(u'Dashboard Status shows correct values for row "Collect Reference Count"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Collect Reference Count"')


@then(u'Dashboard Status shows correct values for row "Dark Reference Count"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Dark Reference Count"')


@then(u'Dashboard Status shows correct values for row "Last White Reference"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Last White Reference"')


@then(u'Dashboard Status shows correct values for row "Last Optimize"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Last Optimize"')


@then(u'Dashboard Status shows correct values for row "Last Dark Reference"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Last Dark Reference"')


@then(u'Dashboard Status shows correct values for row "Last Wavelength Check"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Status shows correct values for row "Last Wavelength Check"')


@then(u'Clicking on Status Refresh should refresh status component')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Clicking on Status Refresh should refresh status component')


@then(u'Dashboard Battery shows Battery or AC Power with correct icon.')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Battery shows Battery or AC Power with correct icon.')


@then(u'Clicking on Battery Refresh should refresh battery component')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Clicking on Battery Refresh should refresh battery component')


@then(u'Dashboard Detector Settings shows correct values for row "VNIR"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Detector Settings shows correct values for row "VNIR"')


@then(u'Dashboard Detector Settings shows correct values for row "SWIR1"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Detector Settings shows correct values for row "SWIR1"')


@then(u'Dashboard Detector Settings shows correct values for row "SWIR2"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard Detector Settings shows correct values for row "SWIR2"')


@then(u'Dashboard GPS shows correct values for row "Fix"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard GPS shows correct values for row "Fix"')


@then(u'Dashboard GPS shows correct values for row "Location"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard GPS shows correct values for row "Location"')


@then(u'Dashboard GPS shows correct values for row "Altitude"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard GPS shows correct values for row "Altitude"')


@then(u'Dashboard GPS shows correct values for row "Format"')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then Dashboard GPS shows correct values for row "Format"')
