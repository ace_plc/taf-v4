from behave import when, then
from testFramework.webapp import webapp, WebApp
from pageObjects.loginPage import loginPage
from steps.common import CommonSteps as cs
from testData.config import settings

commonstp = cs()
@when(u'I go to "Login" page')
def step_impl_load_website(context):
    webapp.load_website()


@when(u'I see login form')
def step_impl_goto_page(context):
    page = str(settings['loginPage'])
    webapp.goto_page(page)

@when(u'I type "{username}" and "{password}"')
def step_impl_click_login_button(context, username,password):
    loginPage.type_username_password(username,password)
    #screenShot = cs()
    commonstp.take_screenshot()


@when(u'I click login button')
def step_impl_click_login_button(context):
    loginPage.login_to_dashboard()


@when(u'I wait for "{sec}" seconds')
def step_impl_wait_in_seconds(context, sec):
    xpathOfElement = "/html/body/app-root/div/div/div/app-provider-dashboard/div[1]/div[2]/div/div/div/a/span"
    #commonstp = cs()
    commonstp.make_a_delay_until_ID_found(sec, xpathOfElement)
    commonstp.take_screenshot()


@when(u'I logout of the site')
def step_impl_click_logout_button(context):
    loginPage.logout_from_site()
    commonstp.take_screenshot()

@then(u'I close browser')
def step_impl_tearDown(self):
    webapp.tearDown()
