from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from testFramework.webapp import webapp
import allure

class CommonSteps():

    def __init__(self):
        self.driver = webapp.get_driver()


    def make_a_delay_until_ID_found(self, sec, xpathOfElement):
        try:
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, xpathOfElement)))
            print "Page is ready!"

        except TimeoutException:
            print "Loading took too much time!"


    def take_screenshot(self):
        allure.attach(self.driver.get_screenshot_as_png(), name='screenshot',
                      attachment_type=allure.attachment_type.PNG)