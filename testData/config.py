import os
import json

settings = None
elements = None


def load_settings():
    global settings
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'settings.json')) as f:
        settings = json.load(f)

def load_html_elements():
    global elements
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'elements.json')) as f:
        elements = json.load(f)


load_settings()
load_html_elements()