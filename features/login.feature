Feature: Login

    Scenario: login Components
        Given I load the website
        When I go to "Login" page
        And I see login form
        And I type "gelila" and "Test@123456"
        And I click login button
        And I wait for "10" seconds
        And I logout of the site
        Then I close browser
